namespace SupermarketCheckout
{
    public class Tests
    {
        Supermarket supermarket = new();
        List<char> products = new();
        PricingRules rules = new MyPricingRules();

        [SetUp]
        public void Setup()
        {
            products.Clear();
            rules.ClearAllProductPrices();
        }

        [Test]
        public void TestCheckoutThrowsArgumentNullException()
        {
            Assert.Multiple(() =>
            {
                Assert.Throws<ArgumentNullException>(() => supermarket.Checkout(null, rules));
                Assert.Throws<ArgumentNullException>(() => supermarket.Checkout(products, null));
            });
        }

        [Test]
        public void TestEmptyCheckout()
        {
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(0));
        }

        [Test]
        public void TestSingleProductCheckout()
        {
            products.Add('A');
            rules.AddProductPrice('A', new UnitPrice(50));
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(50));
        }

        [Test]
        public void TestMultipleOfTheSameProductCheckout()
        {
            products = Enumerable.Repeat('A', 3).ToList();
            rules.AddProductPrice('A', new UnitPrice(50));
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(products.Count * 50));
        }

        [Test]
        public void TestCheckoutReturnsPrecisely()
        {
            products = Enumerable.Repeat('A', 3).ToList();
            rules.AddProductPrice('A', new UnitPrice(50.5));
            // no more than 0.1 % difference between returned value and expected value 
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(151.5).Within(0.1).Ulps);
        }

        [Test]
        public void TestMultipleDistinctProductsCheckoutWithUnitPrices()
        {
            products = new() { 'A', 'C', 'D' };
            rules.AddProductPrice('A', new UnitPrice(50));
            rules.AddProductPrice('C', new UnitPrice(20));
            rules.AddProductPrice('D', new UnitPrice(15));
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(50 + 20 + 15));
        }

        [Test]
        public void TestMultipleRepeatedProductsCheckoutWithUnitPrices()
        {
            products = new() { 'A', 'C', 'D', 'C', 'A' };
            rules.AddProductPrice('A', new UnitPrice(50));
            rules.AddProductPrice('C', new UnitPrice(20));
            rules.AddProductPrice('D', new UnitPrice(15));
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(2*50 + 2*20 + 15));
        }

        [Test]
        public void TestOneProductTypeCheckoutWithSpecialPriceAppliedOnce()
        {
            int specialPriceTriggerCount = 3;
            products = Enumerable.Repeat('A', specialPriceTriggerCount).ToList();
            rules.AddProductPrice('A', new SpecialPrice(50,130,specialPriceTriggerCount));
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(130));
        }

        [Test]
        public void TestOneProductTypeCheckoutWithSpecialPriceAppliedMultipleTimes()
        {
            int specialPriceTriggerCount = 3;
            products = Enumerable.Repeat('A', 2 * specialPriceTriggerCount + 1).ToList();
            rules.AddProductPrice('A', new SpecialPrice(50, 130, 3));
            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(2 * 130 + 50));
        }

        [Test]
        public void TestCheckout()
        {
            // 7 A, 13 B, 4 C, 1D
            products.Add('A');
            products.AddRange(Enumerable.Repeat('C', 3).ToList());
            products.AddRange(Enumerable.Repeat('A', 2*3).ToList());
            products.Add('C');
            products.AddRange(Enumerable.Repeat('B', 6 * 2 + 1).ToList());
            products.Add('D');

            rules.AddProductPrice('A', new SpecialPrice(50, 130, 3));
            rules.AddProductPrice('B', new SpecialPrice(30, 45, 2));
            rules.AddProductPrice('C', new UnitPrice(20));
            rules.AddProductPrice('D', new UnitPrice(15));

            double expected =
                2 * 130 // 2 discounted A
                + 50 // 1 unit A
                + 6 * 45 // 6 discounted B
                + 30  // 1 unit B
                + 4 * 20 // 4 unit C
                + 15; // unit D

            Assert.That(supermarket.Checkout(products, rules), Is.EqualTo(expected));
        }
    }
}