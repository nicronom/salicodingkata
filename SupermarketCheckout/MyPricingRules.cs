﻿namespace SupermarketCheckout
{
    internal class MyPricingRules : PricingRules
    {
        
        public override Dictionary<char, Price> GetPrices()
        {
            return _productPrices;
        }
    }
}
