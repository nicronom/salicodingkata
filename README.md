# Technical task: Software Engineer - Coding Kata

## Author: Sali Hyusein

## Date: 11/08/2023

## HOW TO RUN:

1) Make sure you have .NET 7.0 and SDK v>7.0.1 installed
2) Navigate to the project folder (where SupermarketCheckout.sln resides) in a terminal
3) Execute `dotnet test`

Alternatively open "SupermarketCheckout.sln" in Visual Studio.