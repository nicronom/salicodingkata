﻿namespace SupermarketCheckout
{
    internal class Supermarket
    {
        internal double Checkout(List<char> products, PricingRules rules)
        {
            if (products == null || rules == null) throw new ArgumentNullException();

            Dictionary<char, Price> priceMap = rules.GetPrices();

            // count products of each type
            Dictionary<char, int> productCounts = new();
            foreach (char product in products)
            {
                if (!productCounts.ContainsKey(product)) productCounts[product] = 1;
                else productCounts[product] = productCounts[product] + 1;
            }

            double sum = 0;
            // calculate total price based on rules for each product and add to sum
            foreach (KeyValuePair<char,int> kvp in productCounts)
            {
                if (!priceMap.ContainsKey(kvp.Key)) 
                    throw new Exception("Specified checkout rules do not contain a price for all products listed in checkout");

                sum += priceMap[kvp.Key].GetPrice(kvp.Value);
            }
            return sum;
        }
    }
}