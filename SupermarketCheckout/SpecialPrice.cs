﻿namespace SupermarketCheckout
{
    internal class SpecialPrice : Price
    {
        private readonly double _unitPrice;
        private readonly double _specialPrice;
        private readonly int _specialPriceTriggerCount;
        public SpecialPrice(double unitPrice, double specialPrice, int specialTriggerCount) {
            if (unitPrice < 0 || specialPrice < 0 || specialTriggerCount < 1)
                throw new ArgumentOutOfRangeException(); 

            _unitPrice = unitPrice;
            _specialPrice = specialPrice;
            _specialPriceTriggerCount = specialTriggerCount;
        }

        public override double GetPrice(int productCount)
        {
            if (productCount < 0) throw new ArgumentOutOfRangeException("Product count must be non-negative");
            
            return 
                // number of times the special price is applied
                ((productCount / _specialPriceTriggerCount) * _specialPrice) 
                + 
                // number of times the unit price is applied (remaining items after special discounts) 
                ((productCount % _specialPriceTriggerCount) * _unitPrice);
        }
    }
}
