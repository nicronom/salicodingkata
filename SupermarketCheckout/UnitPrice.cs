﻿namespace SupermarketCheckout
{
    internal class UnitPrice : Price
    {
        private double _price;
        public UnitPrice(double unitPrice) {
            if (unitPrice < 0) throw new ArgumentOutOfRangeException("Price must be non-negative");

            _price = unitPrice;
        }
        public override double GetPrice(int productCount)
        {
            if (productCount < 0) throw new ArgumentOutOfRangeException("Product count must be non-negative");
            return productCount * _price;
        }
    }
}
