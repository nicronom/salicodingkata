﻿namespace SupermarketCheckout
{
    internal abstract class PricingRules
    {
        public abstract Dictionary<char, Price> GetPrices();

        protected Dictionary<char, Price> _productPrices;
        public PricingRules()
        {
            _productPrices = new();
        }

        public PricingRules(Dictionary<char, Price> productPrices)
        {
            _productPrices = productPrices;
        }

        public void AddProductPrice(char product, Price price)
        {
            if (_productPrices.ContainsKey(product)) throw new ArgumentException("Product already exists");
            _productPrices.Add(product, price);
        }

        public Price GetProductPrice(char product)
        {
            if (!_productPrices.ContainsKey(product)) throw new ArgumentException("Product does not exist");
            return _productPrices[product];
        }

        public void RemoveProductPrice(char product)
        {
            _productPrices.Remove(product);
        }

        public void ChangeProductPrice(char product, Price newPrice)
        {
            if (!_productPrices.ContainsKey(product)) throw new ArgumentException("Product does not exist.");
            _productPrices[product] = newPrice;
        }

        public void ClearAllProductPrices()
        {
            _productPrices.Clear();
        }
    }
}