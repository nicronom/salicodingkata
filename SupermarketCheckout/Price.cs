﻿namespace SupermarketCheckout
{
    public abstract class Price
    {
        public abstract double GetPrice(int productCount);
    }
}